# Simple Calculator
This is a simple calculator that can take an arithmetic expression to evaluate and print the result.

## Problem statement
Create a simple calculator which could be used to do basic operators like addition, subtraction, multiplication, division. The calculator should be able to evaluate multi operand expressions and should follow the BODMAS rule. It should also support decimal numbers.
Invalid expressions should be handled gracefully.
 
Example: 

Input
2+3+3
Output
 8

Input
2+3-3
Output
 2

Input
5 / 2 - 3
Output
 -0.5

# How to run
This is a simple calculator that uses node to run the application code.

Assuming you already have node installed. 

First, download all the dependencies by running the following command:
```
npm ci
```

This program is written in Typescript to we need to compile it to JS, to do that run the following command (this will compile and publish the code in `/dist` directory):
```
npm run build
```

You can run the program by `npm start` and providing the arithmetic expression to evaluate as an argument as follows:
```
npm start "2+3+5"
```
_(Above Should give `10` as result)_

You can also clean the generated `/dist` by running:
```
npm run clean
```

For local development I have created a script `npm dev` to clean, build and run the program in a single command as follows:
```
npm run dev "2+3+5"
```
_(Above Should give `10` as result)_

# How to test
We use TDD approach to create a test case and then implement the solution.
You can run all tests by running the following:
```
npm test
```
---

# References and Notes
I have referenced following regarding BODMAS rules:
- https://en.wikipedia.org/wiki/Order_of_operations
- https://www.mathsisfun.com/operation-order-bodmas.html

Note:
- B: Brackets first
- O: Orders (i.e. Powers and Square Roots, etc.)
- DM: Division and Multiplication (left-to-right)
- AS: Addition and Subtraction (left-to-right)

Test Expressions:
>Brackets first: `(3 + 6) × 2 = 18` (*Brackets are not supported in this programme yet*)

>Multiplication before Addition: `3 + 6 * 2 = 15`

>Multiplication and Division rank equally, so just go left to right: `12 / 6 * 3 / 2 = 3`


