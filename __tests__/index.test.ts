import { ExpressionEvaluator } from "../src/expressionEvaluator";
import { Application } from "../src/index"

describe("Application", () => {
    it("should call evaluate of ExpressionEvaluator given commandline argument with expression is available", () => {
        let mockEvaluateFn = jest.fn();
        let mockExpressionEvaluator = {
            Evaluate: mockEvaluateFn
        };
        const oldArgv = process.argv;
        process.argv = [...oldArgv.slice(0, 2), "2+3"];

        Application(mockExpressionEvaluator as unknown as ExpressionEvaluator);

        process.argv = oldArgv

        expect(mockEvaluateFn).toHaveBeenCalled();
    })
})