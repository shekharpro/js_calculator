import { Arithmetics } from "../src/arithmetics";
import { ExpressionEvaluator } from "../src/expressionEvaluator"
import { ArithmeticSymbol } from "../src/arithmeticSymbol"

describe("ArithmeticSymbol", () => {
    describe("ParseExpression", () => {
        it("should parse expression and return expression stack when its valid", () => {
            let expression = "2*3 + 100";
            let evaluator = new ExpressionEvaluator();
            let result = evaluator.ParseExpression(expression);
            expect(result.length).toBe(5);
            expect(result[0]).toEqual(new ArithmeticSymbol('2'));
            expect(result[1]).toEqual(new ArithmeticSymbol('*'));
            expect(result[2]).toEqual(new ArithmeticSymbol('3'));
            expect(result[3]).toEqual(new ArithmeticSymbol('+'));
            expect(result[4]).toEqual(new ArithmeticSymbol('100'));
        });

        it("should throw exception when given expression is not valid", () => {
            let expression = "2*X";
            let evaluator = new ExpressionEvaluator();
            expect(() => evaluator.ParseExpression(expression))
                .toThrow("Given expression 2*X contains invalid arithmetic characters and can't be evaluated")
        });
    });

    describe("Evaluate as per BODMAS rule", () => {
        it("should evaluate a valid expression", () => {
            let expression = "2+3+5";
            let evaluator = new ExpressionEvaluator();
            let result = evaluator.Evaluate(expression);
            expect(result).toEqual(10);
        });
        it("should evaluate Multiplication before Addition for given expression", () => {
            let expression = "3+6*2";
            let evaluator = new ExpressionEvaluator();
            let result = evaluator.Evaluate(expression);
            expect(result).toEqual(15);
        });
        it("should evaluate Division before Addition for given expression", () => {
            let expression = "200+200/4";
            let evaluator = new ExpressionEvaluator();
            let result = evaluator.Evaluate(expression);
            expect(result).toEqual(250);
        });
        it("should evaluate left to right when Multiplication and Division both occur for given expression", () => {
            let expression = "12/6*3/2";
            let evaluator = new ExpressionEvaluator();
            let result = evaluator.Evaluate(expression);
            expect(result).toEqual(3);
        });
        it("should evaluate an expression with all symbols correctly", () => {
            let expression = "10/5+20-2*10/2";
            let evaluator = new ExpressionEvaluator();
            let result = evaluator.Evaluate(expression);
            expect(result).toEqual(12);
        });
    });
});