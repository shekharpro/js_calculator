import { Arithmetics } from "../src/arithmetics"

describe("Numerics", () => {
    describe("parseDigit", () => {
        it("should validate and return the number when given character is digit", () => {
            let [numericCheck, number] = Arithmetics.parseDigit('9');
            expect(numericCheck).toBe(true);
            expect(number).toBe(9);
        });
        it("should invalidate when given character is not numeric", () => {
            let [numericCheck, number] = Arithmetics.parseDigit(''); //force typescript compiler to allow this test
            expect(numericCheck).toBe(false);
            expect(number).toBe(NaN);
        });
        it("should invalidate when given character is not numeric", () => {
            let [numericCheck, number] = Arithmetics.parseDigit('A'); //force typescript compiler to allow this test
            expect(numericCheck).toBe(false);
            expect(number).toBe(NaN);
        });
    })

    describe("parseOperator", () => {
        it("should validate and return the operator function when given character is subtract sign '-'", () => {
            let [operatorCheck, operator, operatorFunction] = Arithmetics.parseOperator('-');
            expect(operatorCheck).toBe(true);
            expect(operator).toBe('-');
            expect(operatorFunction).not.toBeUndefined();
        });
        it("should validate and return the operator function when given character is add sign '+'", () => {
            let [operatorCheck, operator, operatorFunction] = Arithmetics.parseOperator('+');
            expect(operatorCheck).toBe(true);
            expect(operator).toBe('+');
            expect(operatorFunction).not.toBeUndefined();
        });
        it("should validate and return the operator function when given character is multiply sign '*'", () => {
            let [operatorCheck, operator, operatorFunction] = Arithmetics.parseOperator('*');
            expect(operatorCheck).toBe(true);
            expect(operator).toBe('*');
            expect(operatorFunction).not.toBeUndefined();
        });
        it("should validate and return the operator function when given character is divide sign '/'", () => {
            let [operatorCheck, operator, operatorFunction] = Arithmetics.parseOperator('/');
            expect(operatorCheck).toBe(true);
            expect(operator).toBe('/');
            expect(operatorFunction).not.toBeUndefined();
        });
    })

    describe("Operations", () => {
        it("should subtract correctly for Subtract sign '-'", () => {
            let subractFunction = Arithmetics.Operations['-'];
            expect(subractFunction(1,1)).toBe(0);
        });
        it("should add correctly for Add sign '+'", () => {
            let addFunction = Arithmetics.Operations['+'];
            expect(addFunction(1,1)).toBe(2);
        });
        it("should multiply correctly for Multiply sign '*'", () => {
            let multiplyFunction = Arithmetics.Operations['*'];
            expect(multiplyFunction(10,2)).toBe(20);
        });
        it("should divide correctly for Divide sign '/'", () => {
            let divideFunction = Arithmetics.Operations['/'];
            expect(divideFunction(10,2)).toBe(5);
        });
    })
});