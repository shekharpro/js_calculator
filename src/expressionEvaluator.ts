import { ArithmeticFunction, Arithmetics } from "./arithmetics";
import { ArithmeticSymbol } from "./arithmeticSymbol";

export class ExpressionEvaluator {

    Evaluate(expression: string): number {
        let result = 0;
        let expressionStack: ArithmeticSymbol[];
        expressionStack = this.ParseExpression(expression);

        result = this.evaluateExpressionStack(expressionStack);

        return result;
    }

    private evaluateExpressionStack(expressionStack: ArithmeticSymbol[]): number {
        let resultStack: ArithmeticSymbol[] = []
        let operatorsStack: ArithmeticSymbol[] = []
        let index = 0
        while (index < expressionStack.length) {
            if (expressionStack[index].isOperator()) {
                this.processHigherPrecedenceOperators(operatorsStack, expressionStack, index, resultStack);
                operatorsStack.push(expressionStack[index])
                index += 1
            } else {
                resultStack.push(expressionStack[index]);
                index += 1
            }
        }

        this.processLowerPrecedenceOperators(operatorsStack, resultStack);
        return resultStack[0].toNumeric();
    }

    private processLowerPrecedenceOperators(operatorsStack: ArithmeticSymbol[], resultStack: ArithmeticSymbol[]) {
        while (operatorsStack.length > 0) {
            let nextNumber = this.calculateNextNumber(resultStack, operatorsStack);
            resultStack.push(new ArithmeticSymbol(nextNumber.toString()));
        }
    }

    private processHigherPrecedenceOperators(operatorsStack: ArithmeticSymbol[], expressionStack: ArithmeticSymbol[], index: number, resultStack: ArithmeticSymbol[]) {
        while (operatorsStack.length > 0 && Arithmetics.getPrecendence(operatorsStack[operatorsStack.length - 1].toString()) >= Arithmetics.getPrecendence(expressionStack[index].toString())) {
            let nextNumber = this.calculateNextNumber(resultStack, operatorsStack);
            resultStack.push(new ArithmeticSymbol(nextNumber.toString()));
        }
    }

    private calculateNextNumber(resultStack: ArithmeticSymbol[], operatorsStack: ArithmeticSymbol[]): number {
        let secondNumber = resultStack.pop();
        let firstNumber = resultStack.pop();
        let operator = operatorsStack.pop();
        return operator!.applyOperation(firstNumber!, secondNumber!);
    }

    ParseExpression(expression: string): ArithmeticSymbol[] {
        let expressionStack: ArithmeticSymbol[] = [];
        let currentNumber = 0;
        for (let index = 0; index < expression.length; index++) {
            let [isNumber, number] = Arithmetics.parseDigit(expression[index]);
            let [isoperator, operator] = Arithmetics.parseOperator(expression[index]);

            if (expression[index] === ' ') continue;
            
            if (!isNumber && !isoperator) {
                throw Error(`Given expression ${expression} contains invalid arithmetic characters and can't be evaluated`);
            }

            if (isNumber) {
                currentNumber = currentNumber * 10 + number;
                if (index === expression.length - 1) {
                    expressionStack.push(new ArithmeticSymbol(currentNumber.toString()));
                }
            } else {
                expressionStack.push(new ArithmeticSymbol(currentNumber.toString()));
                expressionStack.push(new ArithmeticSymbol(operator))
                currentNumber = 0;
            }
        }

        return expressionStack;
    }

}