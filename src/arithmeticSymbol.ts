import { Arithmetics } from "./arithmetics";


export class ArithmeticSymbol {
    symbol: string;
    constructor(symbol: string) {
        this.symbol = symbol;
    }
    isOperator(): boolean {
        let [isOperator] = Arithmetics.parseOperator(this.symbol);
        return isOperator;
    }
    toNumeric(): number {
        let [_, parsedNumber] = Arithmetics.parseDigit(this.symbol);
        return parsedNumber;
    }
    toString(): string {
        return this.symbol;
    }
    applyOperation(first: ArithmeticSymbol, second: ArithmeticSymbol): number {
        let [_, __, operatorFunction] = Arithmetics.parseOperator(this.symbol);
        let result = operatorFunction!(first.toNumeric(), second.toNumeric());
        return result;
    }
}
