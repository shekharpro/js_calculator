import { ExpressionEvaluator } from "./expressionEvaluator";
export const Application = (expressionEvaliator: ExpressionEvaluator) => {
    if (process.argv && process.argv.length >= 3) {
        let expression = process.argv[2];
        let result = expressionEvaliator.Evaluate(expression);
        console.log(`Input:${expression} \nOutput: ${result}`);
    }
}
Application(new ExpressionEvaluator());