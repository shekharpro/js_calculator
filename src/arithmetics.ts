export type ArithmeticFunction = (a: number, b: number) => number;
export class Arithmetics {
    static parseDigit(character: string): [boolean, number] {
        let parsedValue = parseFloat(character);
        return [!isNaN(parsedValue), parsedValue];
    }

    static parseOperator(character: string): [boolean, string, undefined | ArithmeticFunction] {
        let operatorFunction = this.Operations[character];
        if (operatorFunction) {
            return [true, character, operatorFunction];
        }
        return [false, character, undefined];
    }

    static Operations: { [key: string]: (a: number, b: number) => number } = {
        '-': (a: number, b: number) => a - b,
        '+': (a: number, b: number) => a + b,
        '*': (a: number, b: number) => a * b,
        '/': (a: number, b: number) => a / b,
    };

    static Operators = {
        SUBTRACT : '-',
        ADD : '+',
        MULTIPLY : '*',
        DIVIDE : '/',
    }

    static getPrecendence(operator: string){
        if(operator === this.Operators.ADD || operator === this.Operators.SUBTRACT){
            return 1;
        }
        if(operator === this.Operators.DIVIDE || operator === this.Operators.MULTIPLY){
            return 2;
        }
        return 0;
    }
}